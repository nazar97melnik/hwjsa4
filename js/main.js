"use strict";

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
//AJAX це Asyncronous Java Script And XML , цей підхід програмування дозволяє нам створювати динамічні сторінки , які не потрібно щоразу оновлювати при отриманні інформації, тобто ми можемо довантажити на сторінку контент без її перезавантаження, це дуже зручно насамперед для користувачів, для яких цей сайт і створенно=)

function createPromise(url) {
  return fetch(url).then((response) => response.json());
}

function getFilms(films) {
  const filmsDiv = document.createElement("div");
  document.body.appendChild(filmsDiv);
  films.sort((a, b) => a.episodeId - b.episodeId);
  films.forEach((e) => {
    const { name, episodeId, characters, openingCrawl } = e;
    const div = document.createElement("div");
    div.innerHTML = `
      <p><b>Film Title: ${name}</b></p>
      <p><em>Episode №: ${episodeId} </em></p>
      <p>Description : ${openingCrawl}</p>
      <p>Starring :</p>
      `;
    filmsDiv.appendChild(div);
    Promise.all(characters.map(createPromise))
      .then((actors) => {
        const actorUL = getActor(actors);
        div.appendChild(actorUL);
      })
      .catch((error) => {
        console.error(error);
      });
  });
}

function getActor(characters) {
  const ul = document.createElement("ul");
  characters.forEach((e) => {
    const { name } = e;
    const li = document.createElement("li");
    li.innerText = `${name}`;
    ul.appendChild(li);
  });
  return ul;
}

createPromise(`https://ajax.test-danit.com/api/swapi/films`)
  .then((data) => {
    Promise.all([getFilms(data), getActor(data)]);
  })
  .catch((error) => {
    console.error(error);
  });
